
Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Infra-estrutura para desenvolvimento e implantação de Software (DevOps)*

Prof. Alexander Robert Kutzke

# Exemplo de pipeline que não faz nada :)

- Faça um fork deste projeto;
- Rode seu runner local (se for o caso);
- Registre seu runner local (se for o caso);
- Envie push's para main ou execute manualmente o pipeline.
